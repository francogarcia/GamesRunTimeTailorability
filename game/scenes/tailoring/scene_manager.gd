extends Node

# TODO Background loading
# <http://docs.godotengine.org/en/latest/learning/features/misc/background_loading.html>.

const kMenu = 0
const kProfileCreator = 1
const kGame = 2
const kGameEnd = 3
const kQuit = 4

const kMenuString = "Menu"
const kProfileCreatorString = "Interaction Profile Creator"
const kGameString = "Game"
const kGameEndString = "Game End"
const kQuitString = "Quit"

const kMenuScene = preload("res://scenes/meta_game/abstract_menu.tscn")
const kProfileCreatorScene = preload("res://scenes/user_interface/interaction_profile_creator.tscn")
const kGameScene = preload("res://scenes/meta_game/abstract_game.tscn")
const kGameEndScene = preload("res://scenes/meta_game/abstract_game_end.tscn")

const kGameInput = preload("res://scenes/base_game/base_game_input.tscn")
const kMenuInput = preload("res://scenes/base_game/base_menu_input.tscn")

const kGameOutput = preload("res://scenes/base_game/base_game_output.tscn")
const kMenuOutput = preload("res://scenes/base_game/base_menu_output.tscn")

# NOTE TODO Godot does not allow defining constants based on former constants
# values; therefore, we have to repeat the path here.

# Menu entities.

const kAbstractOption = "option"
const kClassAbstractOption = preload("res://scenes/meta_game/abstract_entities/abstract_option.gd")
const kClassAbstractOptionSceneResource = "res://scenes/meta_game/abstract_entities/abstract_option.tscn"
const kClassAbstractOptionScene = preload("res://scenes/meta_game/abstract_entities/abstract_option.tscn")

# Game entities.

const kAbstractAlien = "alien"
const kClassAbstractAlien = preload("res://scenes/meta_game/abstract_entities/abstract_alien.gd")
const kClassAbstractAlienSceneResource = "res://scenes/meta_game/abstract_entities/abstract_alien.tscn"
const kClassAbstractAlienScene = preload("res://scenes/meta_game/abstract_entities/abstract_alien.tscn")

const kAbstractBomb = "bomb"
# const kClassAbstractBomb = preload("res://scenes/meta_game/abstract_entities/abstract_bomb.gd")
const kClassAbstractBombSceneResource = "res://scenes/meta_game/abstract_entities/abstract_bomb.tscn"
const kClassAbstractBombScene = preload("res://scenes/meta_game/abstract_entities/abstract_bomb.tscn")

const kAbstractBullet = "bullet"
const kClassAbstractBullet = preload("res://scenes/meta_game/abstract_entities/abstract_bullet.gd")
const kClassAbstractBulletSceneResource = "res://scenes/meta_game/abstract_entities/abstract_bullet.tscn"
const kClassAbstractBulletScene = preload("res://scenes/meta_game/abstract_entities/abstract_bullet.tscn")

const kAbstractShield = "shield"
const kClassAbstractShield = preload("res://scenes/meta_game/abstract_entities/abstract_shield.gd")
const kClassAbstractShieldSceneResource = "res://scenes/meta_game/abstract_entities/abstract_shield.tscn"
const kClassAbstractShieldScene = preload("res://scenes/meta_game/abstract_entities/abstract_shield.tscn")

const kAbstractSpaceship = "spaceship"
const kClassAbstractSpaceshipSceneResource = "res://scenes/meta_game/abstract_entities/abstract_spaceship.tscn"



var m_CurrentSceneKey = null
var m_NextSceneKey = null
var m_CurrentSceneName = null

var m_Overlay = null

var m_SharedData = null


func _ready():
    set_fixed_process(true)

    m_CurrentSceneKey = null
    m_NextSceneKey = null
    m_CurrentSceneName = null

    m_SharedData = {
        PlayerAbilities = {

        },
        Scenes = {
            # TODO Use a function to retrieve constant values.
            Menu = {

            },
            Game = {
                LastGameResult = null
            },
            GameEnd = {

            }
        }
    }

    m_Overlay = null

    register_input_actions()


func _fixed_process(delta):
    if (m_NextSceneKey != null):
        m_CurrentSceneKey = m_NextSceneKey
        tailor_game_scene(m_CurrentSceneKey, get_current_scene())
        m_NextSceneKey = null


func register_input_actions():
    # Menus
    InputMap.add_action("confirm")
    InputMap.add_action("next_option")
    InputMap.add_action("previous_option")
    InputMap.add_action("set_option")

    # Game
    InputMap.add_action("spaceship_move_left")
    InputMap.add_action("spaceship_move_right")
    InputMap.add_action("spaceship_fire_bullet")


func instance_abstract_scene(sceneKey):
    # TODO Perhaps we should remove these hard coded strings.
    var abstractScene = null
    if (sceneKey == kMenu):
        abstractScene = kMenuScene
        abstractScene.set_name("AbstractMenu")
    elif (sceneKey == kProfileCreator):
        abstractScene = kProfileCreatorScene
        abstractScene.set_name("ProfileCreator")
    elif (sceneKey == kGame):
        abstractScene = kGameScene
        abstractScene.set_name("AbstractGame")
    elif (sceneKey == kGameEnd):
        abstractScene = kGameEndScene
        abstractScene.set_name("AbstractGameEnd")
    else:
        assert(!"Invalid scene!")

    return abstractScene


func tailor_game_scene(sceneKey, abstractScene):
    # NOTE Currently adding an UI canvas layer to all scenes. It could be
    # instanced per scene instead (in instance_abstract_scene(), for instance).

    # Attach an user-interface overlay to the tailored scene.
    m_Overlay = load("res://scenes/user_interface/user_interface_overlay.tscn").instance()
    m_Overlay.set_layer(1) # It should be above the game's scene.
    # get_current_scene().add_child(m_Overlay)
    abstractScene.add_child(m_Overlay)

    # TODO We could auto-load the overlay scene to reduce the loading overhead.
    # NOTE Provided we ensure the correct execution order, we could remove the
    # conditions at this point. The entire tailoring process is performed in the
    # functions.
    # Wrong order:
    # 1. overlay ready
    # 2. input ready
    # 3. output ready
    # 4. overlay destructed
    # 5. meta game ready
    # Correct order:
    # 1. meta game ready
    # 2. overlay ready
    # 3. input ready
    # 4. output ready
    if (sceneKey == kMenu):
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        # Tailor the game's IO according to the profile.
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    elif (sceneKey == kProfileCreator):
        # TODO The interaction profile creator does not have any tailored
        # version at this time. When implementing this, also implement
        # untailor_game_scene().
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        pass
    elif (sceneKey == kGame):
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        # Tailor the game's IO according to the profile.
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    elif (sceneKey == kGameEnd):
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), "GameEnd")
    else:
        assert(!"Invalid scene!")


func untailor_game_scene(sceneKey, abstractScene):
    # g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    if (sceneKey == kMenu):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    elif (sceneKey == kProfileCreator):
        # The interaction profile creator does not have any tailored version at
        # this time. See tailor_game_scene().
        pass
    elif (sceneKey == kGame):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    elif (sceneKey == kGameEnd):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), "GameEnd")
    else:
        assert(!"Invalid scene!")


func change_scene_to(sceneKey):
    if (m_CurrentSceneKey != null):
        # Remove any tailoring performed to the scene.
        untailor_game_scene(m_CurrentSceneKey, get_current_scene())

    # Prepare next scene.
    m_NextSceneKey = sceneKey

    var abstractScene = instance_abstract_scene(sceneKey)
    get_tree().change_scene_to(abstractScene)

    # NOTE Another option is to load the scene here, and manually add it to the tree:
    # tailor_game_scene(sceneKey, abstractScene)
    # m_CurrentScene.queue_free()
    # get_tree().get_root().add_child(m_CurrentScene)


func attach_scene_to(scene, destinationScene):
    assert(scene != null)
    assert(destinationScene != null)

    var newScene = scene.instance()
    destinationScene.add_child(newScene)


func detach_scene_from(sceneResource, sourceScene):
    assert(sceneResource != null)
    assert(sourceScene != null)

    for child in sourceScene.get_children():
        if (child.get_filename() == sceneResource):
            return child

    return null


func get_current_scene_key():
    return m_CurrentSceneKey


func get_current_scene():
    return get_tree().get_current_scene()


func get_current_scene_name():
    return m_CurrentSceneName


func get_scene_string_from_scene_key(sceneKey):
    if (sceneKey == kMenu):
        return kMenuString
    elif (sceneKey == kProfileCreator):
        return kProfileCreatorString
    elif (sceneKey == kGame):
        return kGameString
    elif (sceneKey == kGameEnd):
        return kGameEndString
    elif (sceneKey == kQuit):
        return kQuitString
    else:
        assert(!"Invalid option!")


func set_shared_scene_data(sceneKey, data):
    var sceneKeyString = get_scene_string_from_scene_key(sceneKey)
    m_SharedData["Scenes"][sceneKeyString] = data


func get_shared_scene_data(sceneKey):
    var sceneKeyString = get_scene_string_from_scene_key(sceneKey)
    return m_SharedData["Scenes"][sceneKeyString]


func get_user_interface_overlay():
    assert(m_Overlay)

    return m_Overlay


func get_abstract_entity_scene_path(sceneKey, entityKey):
    if (sceneKey == kMenu):
        if (entityKey == kAbstractOption):
            return kClassAbstractOptionSceneResource
    elif (sceneKey == kProfileCreator):
        assert(!"Not implemented yet!")
    elif (sceneKey == kGame):
        if (entityKey == kAbstractAlien):
            return kClassAbstractAlienSceneResource
        elif (entityKey == kAbstractBomb):
            return kClassAbstractBombSceneResource
        elif (entityKey == kAbstractBullet):
            return kClassAbstractBulletSceneResource
        elif (entityKey == kAbstractShield):
            return kClassAbstractShieldSceneResource
        elif (entityKey == kAbstractSpaceship):
            return kClassAbstractSpaceshipSceneResource
        else:
            assert(!"Invalid key!")
    elif (sceneKey == kGameEnd):
        assert(!"Not implemented yet!")
    else:
        assert(!"Invalid scene!")


func get_abstract_entity_key(sceneKey, entityScenePath):
    if (sceneKey == kMenu):
        assert(!"Not implemented yet!")
    elif (sceneKey == kProfileCreator):
        assert(!"Not implemented yet!")
    elif (sceneKey == kGame):
        if (entityScenePath == kClassAbstractAlienSceneResource):
            return kAbstractAlien
        elif (entityScenePath == kClassAbstractBombSceneResource):
            return kAbstractBomb
        elif (entityScenePath == kClassAbstractBulletSceneResource):
            return kAbstractBullet
        elif (entityScenePath == kClassAbstractShieldSceneResource):
            return kAbstractShield
        elif (entityScenePath == kClassAbstractSpaceshipSceneResource):
            return kAbstractSpaceship
        else:
            assert(!"Invalid key!")
    elif (sceneKey == kGameEnd):
        assert(!"Not implemented yet!")
    else:
        assert(!"Invalid scene!")


func get_abstract_entity_scene(sceneKey, entityKey):
    if (sceneKey == kMenu):
        assert(!"Not implemented yet!")
    elif (sceneKey == kProfileCreator):
        assert(!"Not implemented yet!")
    elif (sceneKey == kGame):
        if (entityKey == kAbstractAlien):
            return kClassAbstractAlienScene
        elif (entityKey == kAbstractBomb):
            return kClassAbstractBombScene
        elif (entityKey == kAbstractBullet):
            return kClassAbstractBulletScene
        elif (entityKey == kAbstractShield):
            return kClassAbstractShieldScene
        # elif (entityKey == kAbstractSpaceship):
        #     return kClassAbstractSpaceshipScene
        else:
            assert(!"Invalid key!")
    elif (sceneKey == kGameEnd):
        assert(!"Not implemented yet!")
    else:
        assert(!"Invalid scene!")


# NOTE This can be improved to work as a factory pattern.
func instance_abstract_entity_scene(sceneKey, entityKey):
    # TODO Swap here if Godot allows defining constants from other constants in
    # the future. var resource = get_abstract_entity_scene_path(sceneKey, entityKey)
    var resource = get_abstract_entity_scene(sceneKey, entityKey)

    # TODO If expanding for a factory, trigger events for creation and
    # destruction of entities.

    return resource.instance()
