extends Node

# Menu Events

signal EventAutomateConfirm
signal EventAutomateNextOption(offset)
signal EventAutomateSetOption(index)
signal EventAutomateOptionChanged(index)

# Game Events

signal EventAutomateSpaceshipMoveLeft()
signal EventAutomateSpaceshipMoveRight()
signal EventAutomateSpaceshipFire()
