extends Node

func _ready():
    var layout = get_node("CanvasLayer/VBoxContainer")
    layout.set_size(OS.get_window_size())

    var currentScene = g_SceneManager.get_current_scene()
    layout.get_node("LabelResult").set_text(currentScene.get_last_game_result())

    set_fixed_process(true)


func _fixed_process(delta):
    get_node("CanvasLayer/VBoxContainer/LabelTimeLeft").set_text(
        "Returning to menu in %fs." % g_SceneManager.get_current_scene().get_time_left_to_next_scene())
