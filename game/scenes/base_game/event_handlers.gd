extends Node

# TODO It could be useful to move tailor_entity() and untailor_entity() to reuse
# them elsewhere, when needed.

# NOTE As the handlers are called only when the profile is active, checking for
# the existence of the entry could be removed. Keeping for now for safety.

# NOTE Also update untailor_entity() when modifying this.
func tailor_entity(sceneName, entityName, entity):
    var entityComponentTailoring = g_Tailoring.get_interaction_profile()["InteractionProfile"]["Scenes"][sceneName]["ComponentSpecialization"]
    if (entityComponentTailoring.has(entityName)):
        var inputOutputComponents = entityComponentTailoring[entityName]
        for resource in inputOutputComponents:
            g_Tailoring.tailor_suitable_entities(entity,
                                                 g_SceneManager.get_abstract_entity_scene_path(g_SceneManager.kGame, entityName),
                                                 resource)


# NOTE Also update tailor_entity() when modifying this.
func untailor_entity(sceneName, entityName, entity):
    var entityComponentTailoring = g_Tailoring.get_interaction_profile()["InteractionProfile"]["Scenes"][sceneName]["ComponentSpecialization"]
    if (entityComponentTailoring.has(entityName)):
        var inputOutputComponents = entityComponentTailoring[entityName]
        for resource in inputOutputComponents:
            g_Tailoring.untailor_suitable_entities(entity,
                                                   g_SceneManager.get_abstract_entity_scene_path(g_SceneManager.kGame, entityName),
                                                   resource)


# Tailoring-related event handlers.


func on_event_menu_option_changed():
    return


func on_event_alien_spawn(instance):
    tailor_entity(g_SceneManager.get_current_scene_name(), "alien", instance)


func on_event_alien_destroy(instance):
    untailor_entity(g_SceneManager.get_current_scene_name(), "alien", instance)


func on_event_bomb_spawn(instance):
    tailor_entity(g_SceneManager.get_current_scene_name(), "bomb", instance)


func on_event_bomb_destroy(instance):
    untailor_entity(g_SceneManager.get_current_scene_name(), "bomb", instance)


func on_event_bullet_spawn(instance):
    tailor_entity(g_SceneManager.get_current_scene_name(), "bullet", instance)


func on_event_bullet_destroy(instance):
    untailor_entity(g_SceneManager.get_current_scene_name(), "bullet", instance)


func on_event_shield_spawn(instance):
    tailor_entity(g_SceneManager.get_current_scene_name(), "shield", instance)


func on_event_shield_hit(instance):
    pass


func on_event_shield_destroy(instance):
    untailor_entity(g_SceneManager.get_current_scene_name(), "shield", instance)


func on_event_spaceship_spawn(instance):
    tailor_entity(g_SceneManager.get_current_scene_name(), "spaceship", instance)


func on_event_spaceship_hit(instance):
    pass


func on_event_spaceship_destroy(instance):
    pass


# Event handlers proving textual description of the game.


func on_event_menu_option_changed_text_changed_text_description(chosenOption):
    var abstractMenu = g_SceneManager.get_current_scene()
    var chosenOption = abstractMenu.get_selected_option_entity().get_node("ComponentOption").get_description()
    g_SceneManager.get_user_interface_overlay().set_text_description(chosenOption)


func on_event_alien_spawn_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Alien " + str(instance) + " was created!")


func on_event_alien_destroy_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bye, " + str(instance) + "!")


func on_event_bomb_spawn_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bomb " + str(instance) + " was created!")


func on_event_bomb_destroy_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bomb " + str(instance) + " destroyed.")


func on_event_bullet_spawn_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bullet " + str(instance) + " was created!")


func on_event_bullet_destroy_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bullet " + str(instance) + " destroyed.")


func on_event_shield_spawn_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Bullet " + str(instance) + " was created!")


func on_event_shield_hit_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Shield " + str(instance) + " absorved the hit!")


func on_event_shield_destroy_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Shield " + str(instance) + " deactivated.")


func on_event_spaceship_spawn_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Spaceship " + str(instance) + " was created!")


func on_event_spaceship_hit_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Spaceship " + str(instance) + " was hit!")


func on_event_spaceship_destroy_text_description(instance):
    g_SceneManager.get_user_interface_overlay().set_text_description("Spaceship " + str(instance) + " was destroyed.")


# Event handlers proving sound effects to the game.

# TODO Move this to the sounds library.
const kSoundExplosion1 = "explosion1"
const kSoundExplosion2 = "explosion2"
const kSoundExplosion3 = "explosion3"
const kSoundExplosion4 = "explosion4"
const kSoundExplosion5 = "explosion5"
const kSoundExplosion6 = "explosion6"

const kSoundLaser1 = "laser1"
const kSoundLaser2 = "laser2"
const kSoundLaser3 = "laser3"
const kSoundLaser4 = "laser4"
const kSoundLaser5 = "laser5"
const kSoundLaser6 = "laser6"
const kSoundLaser7 = "laser7"
const kSoundLaser8 = "laser8"
const kSoundLaser9 = "laser9"

const kSoundZap1 = "zap1"
const kSoundZap2 = "zap2"
const kSoundZap3 = "zapThreeToneDown"
const kSoundZap4 = "zapThreeToneUp"
const kSoundZap5 = "zapTwoTone"
const kSoundZap6 = "zapTwoTone2"


func on_event_alien_spawn_sound_effect(instance):
    return


func on_event_alien_destroy_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundExplosion1)


func on_event_bomb_spawn_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundLaser2)


func on_event_bomb_destroy_sound_effect(instance):
    return


func on_event_bullet_spawn_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundLaser1)


func on_event_bullet_destroy_sound_effect(instance):
    # TODO Switch to alien hit
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundZap1)


func on_event_shield_spawn_sound_effect(instance):
    return


func on_event_shield_hit_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundZap2)


func on_event_shield_destroy_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundExplosion2)


func on_event_spaceship_spawn_sound_effect(instance):
    return


func on_event_spaceship_hit_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundZap6)


func on_event_spaceship_destroy_sound_effect(instance):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(kSoundExplosion3)


# Input automation.


func should_emit_action(chance):
    assert((chance >= 0.0) and (chance <= 1.0))

    return randf() <= chance


func input_automation_spaceship_move(direction):
    if (should_emit_action(0.3)):
        g_MetaGameEvents.emit_signal("EventSpaceshipMove", direction)


func input_automation_spaceship_move_left():
    input_automation_spaceship_move("left")


func input_automation_spaceship_move_right():
    input_automation_spaceship_move("right")


func input_automation_spaceship_fire():
    if (should_emit_action(0.5)):
        g_MetaGameEvents.emit_signal("EventSpaceshipFire")
