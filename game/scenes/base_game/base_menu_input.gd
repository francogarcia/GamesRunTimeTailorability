extends Node


func _ready():
    set_fixed_process(true)


func _fixed_process(delta):
    if (Input.is_action_just_pressed("confirm")):
        g_MetaGameEvents.emit_signal("EventConfirm")
    if (Input.is_action_just_pressed("next_option")):
        g_MetaGameEvents.emit_signal("EventNextOption", 1)
    if (Input.is_action_just_pressed("previous_option")):
        g_MetaGameEvents.emit_signal("EventNextOption", -1)
    # if (Input.is_action_just_pressed("set_option")):
        # pass
