extends Node



func _ready():
    set_fixed_process(true)


func _fixed_process(delta):
    if (Input.is_action_pressed("spaceship_move_left")):
        g_MetaGameEvents.emit_signal("EventSpaceshipMove", "left")
    if (Input.is_action_pressed("spaceship_move_right")):
        g_MetaGameEvents.emit_signal("EventSpaceshipMove", "right")

    if (Input.is_action_just_pressed("spaceship_fire_bullet")):
        g_MetaGameEvents.emit_signal("EventSpaceshipFire")
