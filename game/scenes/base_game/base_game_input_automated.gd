extends Node

func _ready():
    set_fixed_process(true)


func _fixed_process(delta):
    g_GameEvents.emit_signal("EventAutomateSpaceshipMoveLeft")
    g_GameEvents.emit_signal("EventAutomateSpaceshipMoveRight")
    g_GameEvents.emit_signal("EventAutomateSpaceshipFire")
