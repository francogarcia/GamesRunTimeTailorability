extends Node


const kColorOptionUnselected = Color(1, 1, 1)
const kColorOptionSelected = Color(1, 0, 0)


var m_SelectedOption = null


func _ready():
    var abstractMenu = g_SceneManager.get_current_scene()
    var layout = get_node("CanvasLayer/HBoxContainer")
    layout.set_size(OS.get_window_size())

    # Add a label for each concrete entity.
    var bHasLabel = false
    for entity in abstractMenu.get_children():
        if (entity extends g_SceneManager.kClassAbstractOption):
            var componentOption = entity.get_node("ComponentOption")
            # Add the entity to the layout if this has an output component.
            if (entity.has_node("ComponentUI")):
                var label = Label.new()
                label.set_align(Label.ALIGN_CENTER)
                # label.set_valign(Label.VALIGN_CENTER)
                label.set_text(componentOption.get_description())
                layout.add_child(label)

                bHasLabel = true

    if (bHasLabel):
        g_MetaGameEvents.connect("EventOptionChanged", self, "on_menu_option_changed")
        on_menu_option_changed(abstractMenu.get_selected_option())


# Destructor.
# func _notification(notification):
#    if notification == NOTIFICATION_PREDELETE:
#        g_MetaGameEvents.disconnect("EventOptionChanged", self, "on_menu_option_changed")


func on_menu_option_changed(offset):
    var abstractMenu = g_SceneManager.get_current_scene()
    # var chosenOption = abstractMenu.get_selected_option_entity().get_node("Label").get_text()
    # get_node("CanvasLayer/SelectedOptionLabel").set_text(chosenOption)

    var layout = get_node("CanvasLayer/HBoxContainer")
    var label = null
    if (m_SelectedOption != null):
        label = layout.get_child(m_SelectedOption)
        label.add_color_override("font_color", kColorOptionUnselected)

    m_SelectedOption = abstractMenu.get_selected_option()
    label = layout.get_child(m_SelectedOption)
    label.add_color_override("font_color", kColorOptionSelected)
