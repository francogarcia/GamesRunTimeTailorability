extends Node



func _ready():
    set_fixed_process(true)

    # HUD
    g_MetaGameEvents.connect("EventSpaceshipHit", self, "on_event_spaceship_hit")
    on_event_spaceship_hit(get_tree().get_current_scene().get_node("Spaceship"))


func _fixed_process(delta):
    pass


func on_event_spaceship_hit(instance):
    var currentHealth = instance.get_node("ComponentHealth").current_health()
    var maximumHealth = instance.get_node("ComponentHealth").maximum_health()
    var minimumHealth = instance.get_node("ComponentHealth").minimum_health()

    var healthBar = get_node("CanvasLayer/ProgressBar")
    healthBar.set_max(maximumHealth)
    healthBar.set_min(minimumHealth)
    healthBar.set_value(currentHealth)
