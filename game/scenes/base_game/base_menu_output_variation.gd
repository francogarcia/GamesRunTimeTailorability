extends Node


const kColorOptionUnselected = Color(1, 1, 1)
const kColorOptionSelected = Color(1, 0, 0)


var m_SelectedOption = null


func _ready():
    var abstractMenu = g_SceneManager.get_current_scene()
    var layout = get_node("CanvasLayer/VBoxContainer")
    layout.set_size(OS.get_window_size())

    # Add a label for each concrete entity.
    var bHasButton = false
    for entity in abstractMenu.get_children():
        if (entity extends g_SceneManager.kClassAbstractOption):
            var componentOption = entity.get_node("ComponentOption")
            # Add the entity to the layout if this has an output component.
            if (entity.has_node("ComponentUI")):
                var button = Button.new()
                button.set_text_align(Button.ALIGN_CENTER)
                button.set_text(componentOption.get_description())
                layout.add_child(button)

                bHasButton = true

    if (bHasButton):
        g_MetaGameEvents.connect("EventOptionChanged", self, "on_menu_option_changed")
        on_menu_option_changed(abstractMenu.get_selected_option())

    # Remove Godot's default bindings and mappings for UI interaction.
    for action in ["ui_accept",
                   "ui_select",
                   "ui_cancel",
                   "ui_focuse_next",
                   "ui_focus_prev",
                   "ui_left",
                   "ui_right",
                   "ui_up",
                   "ui_down",
                   "ui_page_up",
                   "ui_page_down"]:
        if InputMap.has_action(action):
            InputMap.erase_action(action)
        InputMap.add_action(action)


# Destructor.
# func _notification(notification):
#    if notification == NOTIFICATION_PREDELETE:
#        g_MetaGameEvents.disconnect("EventOptionChanged", self, "on_menu_option_changed")


func on_menu_option_changed(offset):
    var abstractMenu = g_SceneManager.get_current_scene()
    # var chosenOption = abstractMenu.get_selected_option_entity().get_node("Label").get_text()
    # get_node("CanvasLayer/SelectedOptionLabel").set_text(chosenOption)

    var layout = get_node("CanvasLayer/VBoxContainer")
    var button = null
    if (m_SelectedOption != null):
        button = layout.get_child(m_SelectedOption)
        button.release_focus()

    m_SelectedOption = abstractMenu.get_selected_option()
    button = layout.get_child(m_SelectedOption)
    button.grab_focus()
