extends Node


func _ready():
    # Change scene after ready(), to allow using get_current_scene() on the
    # Tailoring module.
    pass

func _on_Timer_timeout():
    # g_SceneManager.change_scene_to(g_SceneManager.kGame)
    g_SceneManager.change_scene_to(g_SceneManager.kMenu)
