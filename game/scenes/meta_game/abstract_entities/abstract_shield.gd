extends RigidBody2D

const kCollisionShapeSize = Vector2(140, 110)

func _ready():
    var collisionShape = RectangleShape2D.new()
    collisionShape.set_extents(0.5 * kCollisionShapeSize)
    add_shape(collisionShape)
