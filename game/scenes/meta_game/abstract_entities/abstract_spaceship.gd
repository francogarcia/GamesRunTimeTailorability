extends KinematicBody2D

const kCollisionShapeSize = Vector2(100, 80)

func _ready():
    var collisionShape = RectangleShape2D.new()
    collisionShape.set_extents(0.5 * kCollisionShapeSize)
    add_shape(collisionShape)
