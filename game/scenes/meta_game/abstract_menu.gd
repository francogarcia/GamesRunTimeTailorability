extends Node

var m_Options = null
var m_SelectedOption = null


func _ready():
    # Disable 'X' button on window manager to quit the game.
    # get_tree().set_auto_accept_quit(false)

    # Event handlers.
    g_MetaGameEvents.connect("EventConfirm", self, "on_event_confirm")
    g_MetaGameEvents.connect("EventNextOption", self, "on_event_next_option")
    g_MetaGameEvents.connect("EventSetOption", self, "on_event_set_option")

    # Registering available options for the menu.
    m_Options = []

    var option = get_node("OptionNewGame")
    option.get_node("ComponentOption").set_option(g_SceneManager.kGame)
    option.get_node("ComponentOption").set_description(g_SceneManager.kGameString)
    m_Options.push_back(option)

    option = get_node("OptionProfileCreator")
    option.get_node("ComponentOption").set_option(g_SceneManager.kProfileCreator)
    option.get_node("ComponentOption").set_description(g_SceneManager.kProfileCreatorString)
    m_Options.push_back(option)

    option = get_node("OptionQuit")
    option.get_node("ComponentOption").set_option(g_SceneManager.kQuit)
    option.get_node("ComponentOption").set_description(g_SceneManager.kQuitString)
    m_Options.push_back(option)

    # Initial selected option.
    set_selected_option(0)


func quit():
    get_tree().quit()


func cycle_option(offset):
    m_SelectedOption += offset
    if (m_SelectedOption < 0):
        m_SelectedOption += m_Options.size()
    elif (m_SelectedOption >= m_Options.size()):
        m_SelectedOption -= m_Options.size()

    notify_selected_option()


func set_selected_option(index):
    m_SelectedOption = index
    if (m_SelectedOption < 0):
        m_SelectedOption = 0
    elif (m_SelectedOption >= m_Options.size()):
        m_SelectedOption = m_Options.size() - 1

    notify_selected_option()


func get_selected_option():
    return m_SelectedOption


func get_selected_option_entity():
    return m_Options[m_SelectedOption]


func on_event_confirm():
    var chosenOption = m_Options[m_SelectedOption].get_node("ComponentOption").get_option()
    if ((chosenOption == g_SceneManager.kGame) or (chosenOption == g_SceneManager.kProfileCreator)):
        g_SceneManager.change_scene_to(chosenOption)
    elif (chosenOption == g_SceneManager.kQuit):
        quit()
    else:
        assert(!"Invalid index!")


func on_event_next_option(offset):
    cycle_option(offset)


func on_event_set_option(index):
    set_selected_option(index)


func notify_selected_option():
    g_MetaGameEvents.emit_signal("EventOptionChanged", m_SelectedOption)
