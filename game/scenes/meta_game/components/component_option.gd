extends Node

export(int) var m_Option = 0
export(String) var m_Description = ""


func _ready():
    pass


func set_option(optionValue):
    m_Option = optionValue


func get_option():
    return m_Option


func set_description(description):
    m_Description = description


func get_description():
    return m_Description
