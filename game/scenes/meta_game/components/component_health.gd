extends Node

export(int, 0, 100) var m_CurrentValue = 100
export(int, 0, 100) var m_MinimumValue = 0
export(int, 0, 100) var m_MaximumValue = 100


func _ready():
    pass


func reduce_health(value):
    assert(is_alive())

    if (m_CurrentValue > m_MinimumValue):
        m_CurrentValue -= value
        if (m_CurrentValue < m_MinimumValue):
            m_CurrentValue = m_MinimumValue


func increase_health(value):
    m_CurrentValue += value
    if (m_CurrentValue > m_MaximumValue):
        m_CurrentValue = m_MaximumValue


func current_health():
    return m_CurrentValue


func maximum_health():
    return m_MaximumValue


func minimum_health():
    return m_MinimumValue


func is_alive():
    return (m_CurrentValue > m_MinimumValue)
