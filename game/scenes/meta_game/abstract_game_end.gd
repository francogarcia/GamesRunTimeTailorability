extends Node


const kTimeToReturnToMenuSeconds = 1.0


var m_LastGameResult = null


func _ready():
    # Change scene after ready(), to allow using get_current_scene() on the
    # Tailoring module.

    var sharedData = g_SceneManager.get_shared_scene_data(g_SceneManager.kGame)
    m_LastGameResult = sharedData["LastGameState"]

    var timer = get_node("Timer")
    timer.set_wait_time(kTimeToReturnToMenuSeconds)


func get_last_game_result():
    return m_LastGameResult


func get_time_left_to_next_scene():
    return get_node("Timer").get_time_left()


func play_again():
    g_SceneManager.change_scene_to(g_SceneManager.kGame)


func return_to_menu():
    g_SceneManager.change_scene_to(g_SceneManager.kMenu)


func _on_Timer_timeout():
    # play_again()
    return_to_menu()
