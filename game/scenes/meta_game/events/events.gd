extends Node

# Menu Events

signal EventConfirm
signal EventNextOption(offset)
signal EventSetOption(index)
signal EventOptionChanged(index)

# Game Events

signal EventSpaceshipSpawn(instance)
signal EventSpaceshipMove(direction)
signal EventSpaceshipFire
signal EventSpaceshipHit(instance)
signal EventSpaceshipDestroy(instance)

signal EventAlienSpawn(instance)
signal EventAlienMove(direction)
signal EventAlienFire
signal EventAlienDestroy(instance)

signal EventBulletSpawn(instance)
signal EventBulletDestroy(instance)

signal EventBombSpawn(instance)
signal EventBombDestroy(instance)

signal EventShieldSpawn(instance)
signal EventShieldHit(instance)
signal EventShieldDestroy(instance)

#func register_event_handler(signalName, sceneInstance, eventHandlerName):
#	connect(signalName, sceneInstance, eventHandlerName)
