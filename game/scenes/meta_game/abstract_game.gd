extends Node

const kStateRunning = 0
const kStateVictory = 1
const kStateDefeat = 2

# Pixels/second.
const kSpaceshipLinearVelocity = 200
const kEnemyLinearVelocity = 500

# Pixels/second.
const kBulletVelocity = Vector2(0, -400)
const kBombVelocity = Vector2(0, 100)
# Pixels.
const kBulletMinimumHeight = 0
const kBombMaximumHeight = 1000
# In seconds.
const kBulletCooldownTime = 1
const kBulletAvailableTime = 0

var kTotalAliens = 5
var kAlienSpacing = 2

var kTotalShields = 2
var kShieldSpacing = 3

var m_BulletCooldownTime
var m_Motion
var m_bSpaceshipFireRequest

var m_AliensRemaining
var m_GameState

var m_AliensToDestroy
var m_BombsToDestroy
var m_BulletsToDestroy
var m_ShieldsToDestroy


func _ready():
    set_fixed_process(true)

    m_BulletCooldownTime = kBulletAvailableTime
    m_Motion = Vector2(0, 0)
    m_bSpaceshipFireRequest = false

    m_GameState = kStateRunning

    m_AliensToDestroy = []
    m_BombsToDestroy = []
    m_BulletsToDestroy = []
    m_ShieldsToDestroy = []

    g_MetaGameEvents.emit_signal("EventSpaceshipSpawn", get_node("Spaceship"))

    m_AliensRemaining = kTotalAliens
    for alienIndex in range(0, kTotalAliens):
        var alien = g_SceneManager.instance_abstract_entity_scene(g_SceneManager.kGame, g_SceneManager.kAbstractAlien)
        var position = Vector2(0, 0)
        position.x = alienIndex * (alien.kCollisionShapeSize.x) * kAlienSpacing
        position.y = alien.kCollisionShapeSize.y
        alien.set_pos(position)
        get_node("Enemies").add_child(alien)

        g_MetaGameEvents.emit_signal("EventAlienSpawn", alien)

    var spaceshipPosition = get_node("Spaceship").get_pos()
    spaceshipPosition.x /= kTotalShields
    for shieldIndex in range(0, kTotalShields):
        var shield = g_SceneManager.instance_abstract_entity_scene(g_SceneManager.kGame, g_SceneManager.kAbstractShield)
        var position = Vector2(0, 0)
        position.x = spaceshipPosition.x + shieldIndex * (shield.kCollisionShapeSize.x) * kShieldSpacing
        position.y = spaceshipPosition.y - shield.kCollisionShapeSize.y
        shield.set_pos(position)
        get_node("Shields").add_child(shield)

        g_MetaGameEvents.emit_signal("EventShieldSpawn", shield)

    # Event handlers.
    g_MetaGameEvents.connect("EventSpaceshipSpawn", self, "on_event_spaceship_spawn")
    g_MetaGameEvents.connect("EventSpaceshipMove", self, "on_event_spaceship_move")
    g_MetaGameEvents.connect("EventSpaceshipFire", self, "on_event_spaceship_fire")
    g_MetaGameEvents.connect("EventSpaceshipDestroy", self, "on_event_spaceship_destroy")

    g_MetaGameEvents.connect("EventAlienSpawn", self, "on_event_alien_spawn")
    g_MetaGameEvents.connect("EventAlienMove", self, "on_event_alien_move")
    g_MetaGameEvents.connect("EventAlienFire", self, "on_event_alien_fire")
    g_MetaGameEvents.connect("EventAlienDestroy", self, "on_event_alien_destroy")


func on_event_spaceship_spawn(instance):
    pass


func on_event_spaceship_move(direction):
    if (direction == "left"):
        m_Motion += Vector2(-1, 0)
    elif (direction == "right"):
        m_Motion += Vector2(1, 0)


func on_event_spaceship_fire():
    m_bSpaceshipFireRequest = true


func on_event_spaceship_destroy(instance):
    m_GameState = kStateDefeat


func on_event_alien_spawn(instance):
    pass


func on_event_alien_move(direction):
    pass


func on_event_alien_fire():
    pass


func on_event_alien_destroy(instance):
    m_AliensRemaining -= 1
    if (m_AliensRemaining <= 0):
        m_GameState = kStateVictory


func _fixed_process(delta):
    var spaceship = get_node("Spaceship")
    spaceship.move(m_Motion * kSpaceshipLinearVelocity * delta)
    m_Motion.x = 0
    m_Motion.y = 0

    m_BulletCooldownTime -= delta
    if ((m_BulletCooldownTime <= kBulletAvailableTime) and (m_bSpaceshipFireRequest)):
        var bullet = g_SceneManager.instance_abstract_entity_scene(g_SceneManager.kGame, g_SceneManager.kAbstractBullet)

        var position = spaceship.get_global_pos()
        position.y -= (spaceship.kCollisionShapeSize.y / 2 + bullet.kCollisionShapeSize.y / 2)
        bullet.set_pos(position)
        get_node("Bullets").add_child(bullet)

        bullet.connect("area_enter", self, "on_bullet_collision")

        g_MetaGameEvents.emit_signal("EventBulletSpawn", bullet)

        m_BulletCooldownTime = kBulletCooldownTime
        m_bSpaceshipFireRequest = false

    var entitiesCount = get_node("Bullets").get_child_count()
    for entityIndex in range(0, entitiesCount):
        var position = get_node("Bullets").get_child(entityIndex).get_pos()
        position += (kBulletVelocity * delta)

        # Remove bullets far away from the playing area.
        if (position.y > kBulletMinimumHeight):
            get_node("Bullets").get_child(entityIndex).set_pos(position)
        else:
            g_MetaGameEvents.emit_signal("EventBulletDestroy", get_node("Bullets").get_child(entityIndex))
            get_node("Bullets").get_child(entityIndex).queue_free()

    var entitiesCount = get_node("Bombs").get_child_count()
    for entityIndex in range(0, entitiesCount):
        var position = get_node("Bombs").get_child(entityIndex).get_pos()
        position += (kBombVelocity * delta)

        # Remove bombs far away from the playing area.
        if (position.y < kBombMaximumHeight):
            get_node("Bombs").get_child(entityIndex).set_pos(position)
        else:
            g_MetaGameEvents.emit_signal("EventBombDestroy", get_node("Bombs").get_child(entityIndex))
            get_node("Bombs").get_child(entityIndex).queue_free()

    entitiesCount = get_node("Enemies").get_child_count()
    for entityIndex in range(0, entitiesCount):
        var bFire = ((randi() % 1000) < 2)
        if (bFire):
            var bomb = g_SceneManager.instance_abstract_entity_scene(g_SceneManager.kGame, g_SceneManager.kAbstractBomb)

            var position = get_node("Enemies").get_child(entityIndex).get_global_pos()
            position.y += (get_node("Enemies").get_child(entityIndex).kCollisionShapeSize.y / 2 + bomb.kCollisionShapeSize.y / 2)
            bomb.set_pos(position)
            get_node("Bombs").add_child(bomb)

            bomb.connect("body_enter", self, "on_bomb_collision")

            g_MetaGameEvents.emit_signal("EventBombSpawn", bomb)

        # Movement
        var position = get_node("Enemies").get_child(entityIndex).get_pos()
        var moveTo = randi() % 1000
        if (moveTo < 10):
            position.x += kEnemyLinearVelocity * delta
        elif (moveTo < 20):
            position.x -= kEnemyLinearVelocity * delta
        get_node("Enemies").get_child(entityIndex).set_pos(position)

    while (not m_AliensToDestroy.empty()):
        m_AliensToDestroy[0].queue_free()
        m_AliensToDestroy.pop_front()

    while (not m_BombsToDestroy.empty()):
        m_BombsToDestroy[0].queue_free()
        m_BombsToDestroy.pop_front()

    while (not m_BulletsToDestroy.empty()):
        m_BulletsToDestroy[0].queue_free()
        m_BulletsToDestroy.pop_front()

    while (not m_ShieldsToDestroy.empty()):
        m_ShieldsToDestroy[0].queue_free()
        m_ShieldsToDestroy.pop_front()

    # Change scene after the game ended.
    if (m_GameState != kStateRunning):
        g_SceneManager.change_scene_to(g_SceneManager.kGameEnd)

        var sharedData = g_SceneManager.get_shared_scene_data(g_SceneManager.kGame)
        if (m_GameState == kStateVictory):
            sharedData["LastGameState"] = "Victory"
        else:
            sharedData["LastGameState"] = "Defeat"

        g_SceneManager.set_shared_scene_data(g_SceneManager.kGame, sharedData)


func on_bomb_collision(body):
    #print(body.get_colliding_bodies())
    #print(body.get_collider())
    var entitiesCount = get_node("Bombs").get_child_count()
    for entityIndex in range(0, entitiesCount):
        if (get_node("Bombs").get_child(entityIndex).overlaps_body(body)):
            var bomb = get_node("Bombs").get_child(entityIndex)
            var damage = bomb.get_node("ComponentDamage").inflicted_damage()
            var health = body.get_node("ComponentHealth")

            assert(health.is_alive())

            if (health.is_alive()):
                body.get_node("ComponentHealth").reduce_health(damage)

            if (body == get_node("Spaceship")):
                g_MetaGameEvents.emit_signal("EventSpaceshipHit", body)
                if (not health.is_alive()):
                    g_MetaGameEvents.emit_signal("EventSpaceshipDestroy", body)
            elif (body extends g_SceneManager.kClassAbstractShield):
                g_MetaGameEvents.emit_signal("EventShieldHit", body)
                if (not health.is_alive()):
                    g_MetaGameEvents.emit_signal("EventShieldDestroy", body)
                    m_ShieldsToDestroy.push_back(body)

            m_BombsToDestroy.push_back(bomb)
            g_MetaGameEvents.emit_signal("EventBombDestroy", bomb)

            return


func on_bullet_collision(body):
    var entitiesCount = get_node("Bullets").get_child_count()
    for entityIndex in range(0, entitiesCount):
        if (body.overlaps_area(get_node("Bullets").get_child(entityIndex))):
            if (body extends g_SceneManager.kClassAbstractAlien):
                var bullet = get_node("Bullets").get_child(entityIndex)
                var damage = bullet.get_node("ComponentDamage").inflicted_damage()
                var health = body.get_node("ComponentHealth")

                assert(health.is_alive())

                if (health.is_alive()):
                    body.get_node("ComponentHealth").reduce_health(damage)

                if (not health.is_alive()):
                    m_AliensToDestroy.push_back(body)
                    g_MetaGameEvents.emit_signal("EventAlienDestroy", body)

                m_BulletsToDestroy.push_back(bullet)
                g_MetaGameEvents.emit_signal("EventBulletDestroy", bullet)

                return
