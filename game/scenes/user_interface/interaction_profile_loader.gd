extends Button


func _ready():
    var fileDialog = get_node("FileDialog")
    fileDialog.add_filter("*.json; Interaction Profile Files")


func load_interaction_profile(interactionProfileFilename):
    g_SceneManager.get_user_interface_overlay().set_custom_interaction_profile_path(interactionProfileFilename)
    # Tailor the game with the selected profile.
    g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    g_Tailoring.load_and_set_interaction_profile(interactionProfileFilename)
    g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    # TODO More idiomatic ways:
    # Input.action_press("load_profile_custom")
    # Input.action_release("load_profile_custom")
    # or
    # var inputEvent = InputEvent()
    # inputEvent.type = InputEvent.ACTION
    # inputEvent.set_as_action("load_profile_custom", true)
    # get_tree().input_event(inputEvent)

func _on_ButtonLoadProfile_pressed():
    var fileDialog = get_node("FileDialog")
    fileDialog.invalidate()
    fileDialog.popup_centered_ratio(0.75)


func _on_FileDialog_file_selected(path):
    load_interaction_profile(path)


func _on_ButtonCancel_pressed():
    pass
