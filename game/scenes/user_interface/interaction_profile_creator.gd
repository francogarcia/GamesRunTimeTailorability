extends Node

const kTabEntity = "Entities"
const kTabEvent = "Events"
const kTabGameCommand = "Game Commands"
const kTabInputSubsystem = "Input Subsystems"
const kTabOutputSubsystem = "Output Subsystems"
const kTabInputDevice = "Input Device"

const kConcreteEntities = {
    Game = {
        spaceship = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_spaceship.tscn",
            "res://scenes/usual_game/concrete_entities/concrete_usual_spaceship_variation.tscn"
        ],
        bullet = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_bullet.tscn",
            "res://scenes/usual_game/concrete_entities/concrete_usual_bullet_variation.tscn"
        ],
        bomb = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_bomb.tscn",
            "res://scenes/usual_game/concrete_entities/concrete_usual_bomb_variation.tscn"
        ],
        shield = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_shield.tscn",
            "res://scenes/usual_game/concrete_entities/concrete_usual_shield_variation.tscn"
        ],
        alien = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_alien.tscn",
            "res://scenes/usual_game/concrete_entities/concrete_usual_alien_variation.tscn"
        ]
    },
    GameEnd = {
    },
    Menu = {
        option = [
            "res://scenes/usual_game/concrete_entities/concrete_usual_option.tscn"
        ]
    }
}

const kConcreteEventHandlers = {
    Game = {
        EventAlienSpawn = [
            "on_event_alien_spawn",
            "on_event_spaceship_spawn_text_description",
            "on_event_alien_spawn_sound_effect"
        ],
        EventAlienDestroy = [
            "on_event_alien_destroy",
            "on_event_bullet_spawn_text_description",
            "on_event_alien_destroy_sound_effect"
        ],

        EventBombSpawn = [
            "on_event_bomb_spawn",
            "on_event_bomb_spawn_text_description",
            "on_event_bomb_spawn_sound_effect"
        ],
        EventBombDestroy = [
            "on_event_bomb_destroy",
            "on_event_bomb_destroy_text_description",
            "on_event_bomb_destroy_sound_effect"
        ],

        EventBulletSpawn = [
            "on_event_bullet_spawn",
            "on_event_bullet_spawn_text_description",
            "on_event_bullet_spawn_sound_effect"
        ],
        EventBulletDestroy = [
            "on_event_bullet_destroy",
            "on_event_bullet_destroy_text_description",
            "on_event_bullet_destroy_sound_effect"
        ],

        EventShieldSpawn = [
            "on_event_shield_spawn",
            "on_event_shield_spawn_text_description",
            "on_event_shield_spawn_sound_effect"
        ],
        EventShieldHit = [
            "on_event_shield_hit",
            "on_event_shield_hit_text_description",
            "on_event_shield_hit_sound_effect"
        ],
        EventShieldDestroy = [
            "on_event_shield_destroy",
            "on_event_shield_destroy_text_description",
            "on_event_shield_destroy_sound_effect"
        ],

        EventSpaceshipSpawn = [
            "on_event_spaceship_spawn",
            "on_event_spaceship_spawn_text_description",
            "on_event_spaceship_spawn_sound_effect"
        ],
        EventSpaceshipHit = [
            "on_event_spaceship_hit",
            "on_event_spaceship_hit_text_description",
            "on_event_spaceship_hit_sound_effect"
        ],
        EventSpaceshipDestroy = [
            "on_event_spaceship_destroy",
            "on_event_spaceship_destroy_text_description",
            "on_event_spaceship_destroy_sound_effect"
        ]
    },
    GameEnd = {
    },
    Menu = {
        EventOptionChanged = [
            "on_event_menu_option_changed_text_changed_text_description"
        ]
    }
}

const kInputSubsystems = {
    Game = {
        usual_game_input = "res://scenes/base_game/base_game_input.tscn",
        usual_game_input_automated = "res://scenes/base_game/base_game_input_automated.tscn"
    },
    GameEnd = {
    },
    Menu = {
        usual_menu_input = "res://scenes/base_game/base_menu_input.tscn",
    }
}

const kOutputSubsystems = {
    Game = {
        usual_game_output = "res://scenes/base_game/base_game_output.tscn"
    },
    GameEnd = {
        usual_game_end_output = "res://scenes/base_game/base_game_end_output.tscn"
    },
    Menu = {
        usual_menu_output = "res://scenes/base_game/base_menu_output.tscn",
        usual_menu_output_variation = "res://scenes/base_game/base_menu_output_variation.tscn"
    }
}

const kGameCommands = {
    Game = [
        "spaceship_move_left",
        "spaceship_move_right",
        "spaceship_fire_bullet"
    ],
    GameEnd = {
    },
    Menu = [
        "confirm",
        "next_option",
        "previous_option"
    ]
}

const kInputDevices = {
    Game = {
        Automation = {
            spaceship_move_left = {
                event = "EventAutomateSpaceshipMoveLeft",
                handlers = [
                    "input_automation_spaceship_move_left"
                ]
            },
            spaceship_move_right = {
                event = "EventAutomateSpaceshipMoveRight",
                handlers = [
                    "input_automation_spaceship_move_right"
                ]
            },
            spaceship_fire_bullet = {
                event = "EventAutomateSpaceshipFire",
                handlers = [
                    "input_automation_spaceship_fire"
                ]
            }
        },
        Controller = {
        },
        Keyboard = {
        },
        Mouse = {
        }
    },
    GameEnd = {
        Automation = {
        },
        Controller = {
        },
        Keyboard = {
        },
        Mouse = {
        }
    },
    Menu = {
        Automation = {
        },
        Controller = {
        },
        Keyboard = {
        },
        Mouse = {
        }
    }
}

const kDefaultInputMapping = {
    Game = {
        "spaceship_move_left": ["Keyboard", "A"],
        "spaceship_move_right": ["Keyboard", "D"],
        # "spaceship_move_right": ["Automation", "EventAutomateSpaceshipMoveRight", "input_automation_spaceship_move_right"],
        "spaceship_fire_bullet": ["Keyboard", "SPACE"]
        # spaceship_fire_bullet = ["Mouse Button", "Left"]
    },
    GameEnd = {
    },
    Menu = {
        "confirm": ["Keyboard", "RETURN"],
        "next_option": ["Keyboard", "D"],
        "previous_option": ["Keyboard", "A"]
    }
}

const kInputDeviceMouseOptions = [
    ["Left Button", "Left"],
    ["Right Button", "Right"],
    ["Middle Button", "Middle"],
    ["Wheel Up", "Wheel Up"],
    ["Wheel Down", "Wheel Down"],
    ["Button 6", "Button 6"],
    ["Button 7", "Button 7"],
    ["Button 8", "Button 8"],
    ["Button 9", "Button 9"]
]

const kInputDeviceControllerOptions = [
    ["PS Cross, XBox A, Nintendo B", "Face Button Bottom"],
    ["PS Circle, XBox B, Nintendo A", "Face Button Right"],
    ["PS Square, XBox X, Nintendo Y", "Face Button Left"],
    ["PS Triangle, XBox Y, Nintendo X", "Face Button Top"],
    ["L, L1", "L"],
    ["R, R1", "R"],
    ["L2", "L2"],
    ["R2", "R2"],
    ["L3", "L3"],
    ["R3", "R3"],
    ["Select", "Select"],
    ["Start", "Start"],
    ["D-Pad Up", "DPAD Up"],
    ["D-Pad Down", "DPAD Down"],
    ["D-Pad Left", "DPAD Left"],
    ["D-Pad Right", "DPAD Right"]
]


var m_SelectedContreteEntities = null
var m_SelectedConcreteEventHandlers = null
var m_SelectedInputMapping = null
var m_SelectedInputSubsystems = null
var m_SelectedOutputSubsystems = null

var m_SelectedInputMappingKeyboardKey = null


func _ready():
    # Defaults: initial configuration.
    m_SelectedContreteEntities = {
        Game = {
            spaceship = [
            ],
            bullet = [
            ],
            bomb = [
            ],
            shield = [
            ],
            alien = [
            ]
        },
        GameEnd = {
        },
        Menu = {
            option = [
            ]
        }
    }

    m_SelectedConcreteEventHandlers = {
        Game = {
            EventAlienSpawn = [
            ],
            EventAlienDestroy = [
            ],

            EventBombSpawn = [
            ],
            EventBombDestroy = [
            ],

            EventBulletSpawn = [
            ],
            EventBulletDestroy = [
            ],

            EventShieldSpawn = [
            ],
            EventShieldHit = [
            ],
            EventShieldDestroy = [
            ],

            EventSpaceshipSpawn = [
            ],
            EventSpaceshipHit = [
            ],
            EventSpaceshipDestroy = [
            ]
        },
        GameEnd = {
        },
        Menu = {
            EventOptionChanged = [
            ]
        }
    }

    # TODO Copy from kDefaultInputMapping when updating to Godot 3.
    m_SelectedInputMapping = {
        Game = {
            "spaceship_move_left": ["Keyboard", "A"],
            "spaceship_move_right": ["Keyboard", "D"],
            "spaceship_fire_bullet": ["Keyboard", "SPACE"]
        },
        GameEnd = {
        },
        Menu = {
            "confirm": ["Keyboard", "RETURN"],
            "next_option": ["Keyboard", "D"],
            "previous_option": ["Keyboard", "A"]
        }
    }

    m_SelectedInputSubsystems = {
        Game = {
        },
        GameEnd = {
        },
        Menu = {
        }
    }

    m_SelectedOutputSubsystems = {
        Game = {
        },
        GameEnd = {
        },
        Menu = {
        }
    }

    m_SelectedInputMappingKeyboardKey = null

    # Creating the user interface.

    # set_size(OS.get_window_size())
    set_size(get_viewport().get_rect().size)

    var tabContainer = get_node("TabContainer")
    var tabNames = [kTabEntity, kTabEvent, kTabGameCommand, kTabInputSubsystem, kTabOutputSubsystem]
    for tabName in tabNames:
        var tab = Tabs.new()
        tab.set_name(tabName)
        tab.set_anchor(MARGIN_LEFT, Control.ANCHOR_BEGIN)
        tab.set_anchor(MARGIN_TOP, Control.ANCHOR_BEGIN)
        tab.set_anchor(MARGIN_RIGHT, Control.ANCHOR_END)
        tab.set_anchor(MARGIN_BOTTOM, Control.ANCHOR_END)
        # tab.set_h_size_flags(Control.SIZE_FILL)
        # tab.set_v_size_flags(Control.SIZE_EXPAND_FILL)
        tabContainer.add_child(tab)

        # TODO Fill the entire area of the tab.
        var container = ScrollContainer.new()
        container.set_anchor(MARGIN_LEFT, Control.ANCHOR_BEGIN)
        container.set_anchor(MARGIN_TOP, Control.ANCHOR_BEGIN)
        container.set_anchor(MARGIN_RIGHT, Control.ANCHOR_END)
        container.set_anchor(MARGIN_BOTTOM, Control.ANCHOR_END)
        # container.set_h_size_flags(Control.SIZE_FILL)
        # container.set_v_size_flags(Control.SIZE_FILL)
        container.set_h_scroll(true)
        container.set_v_scroll(true)
        tab.add_child(container)

        var layout = VBoxContainer.new()
        layout.set_anchor(MARGIN_LEFT, Control.ANCHOR_BEGIN)
        layout.set_anchor(MARGIN_TOP, Control.ANCHOR_BEGIN)
        layout.set_anchor(MARGIN_RIGHT, Control.ANCHOR_END)
        layout.set_anchor(MARGIN_BOTTOM, Control.ANCHOR_END)
        # layout.set_h_size_flags(Control.SIZE_EXPAND_FILL)
        # layout.set_v_size_flags(Control.SIZE_EXPAND_FILL)
        layout.add_constant_override("separation", 15)
        container.add_child(layout)

        if (tabName == kTabEntity):
            for sceneName in kConcreteEntities:
                add_label_to_layout(layout, sceneName)
                layout.add_child(HSeparator.new())

                var gameScene = kConcreteEntities[sceneName]
                for abstractEntityName in gameScene:
                    add_label_to_layout(layout, abstractEntityName)

                    var toggleLayout = VBoxContainer.new()
                    layout.add_child(toggleLayout)

                    var concreteEntities = gameScene[abstractEntityName]
                    for concreteEntityResource in concreteEntities:
                        add_toggle_option_to_layout(toggleLayout,
                                                    concreteEntityResource,
                                                    false,
                                                    {
                                                        type = kTabEntity,
                                                        scene = sceneName,
                                                        target = abstractEntityName
                                                    })
        elif (tabName == kTabEvent):
            for sceneName in kConcreteEventHandlers:
                add_label_to_layout(layout, sceneName)
                layout.add_child(HSeparator.new())

                var gameScene = kConcreteEventHandlers[sceneName]
                for eventName in gameScene:
                    add_label_to_layout(layout, eventName)

                    var toggleLayout = VBoxContainer.new()
                    layout.add_child(toggleLayout)

                    var concreteEventHandlers = gameScene[eventName]
                    for concreteEventHandlerName in concreteEventHandlers:
                        add_toggle_option_to_layout(toggleLayout,
                                                    concreteEventHandlerName,
                                                    false,
                                                    {
                                                        type = kTabEvent,
                                                        scene = sceneName,
                                                        target = eventName
                                                    })
        elif (tabName == kTabGameCommand):
            for sceneName in kGameCommands:
                add_label_to_layout(layout, sceneName)
                layout.add_child(HSeparator.new())

                var commandBindingLayout = GridContainer.new()
                commandBindingLayout.set_anchor(MARGIN_LEFT, Control.ANCHOR_BEGIN)
                commandBindingLayout.set_anchor(MARGIN_TOP, Control.ANCHOR_BEGIN)
                commandBindingLayout.set_anchor(MARGIN_RIGHT, Control.ANCHOR_END)
                commandBindingLayout.set_anchor(MARGIN_BOTTOM, Control.ANCHOR_END)
                commandBindingLayout.set_columns(3)
                # commandBindingLayout.set_h_size_flags(Control.SIZE_EXPAND_FILL)
                # commandBindingLayout.set_v_size_flags(Control.SIZE_EXPAND_FILL)
                # TODO Work around as layouts are not filling the entire tab area.
                commandBindingLayout.add_constant_override("hseparation", 50)
                layout.add_child(commandBindingLayout)

                var sceneInputDevices = kInputDevices[sceneName].keys()
                var sceneAutomation = kInputDevices[sceneName]["Automation"]
                var sceneCommands = kGameCommands[sceneName]
                for gameCommandName in sceneCommands:
                    var commandAutomation = null
                    if (sceneAutomation.has(gameCommandName)):
                        commandAutomation = sceneAutomation[gameCommandName]

                    var currentDevice = m_SelectedInputMapping[sceneName][gameCommandName][0]
                    var optionButton = OptionButton.new()
                    var selectedOptionIndex = null
                    # NOTE Assuming we only have one mapping, as we are
                    # retrieving the first.
                    for inputDevice in sceneInputDevices:
                        if (inputDevice == "Automation"):
                            if (commandAutomation != null):
                                # There is at least one option for automation.
                                optionButton.add_item(inputDevice)
                        else:
                            optionButton.add_item(inputDevice)

                        # Select the item which matches the selected input
                        # device option in the profile.
                        if (inputDevice == currentDevice):
                            selectedOptionIndex = optionButton.get_item_count() - 1
                            optionButton.select(selectedOptionIndex)

                    add_label_to_layout(commandBindingLayout, gameCommandName)
                    commandBindingLayout.add_child(optionButton)
                    # - 1 would be the last element index, we want the next (the one we will create).
                    var gridIndex = commandBindingLayout.get_child_count()
                    on_input_device_options_item_selected(selectedOptionIndex, optionButton, commandBindingLayout, gridIndex, sceneName, gameCommandName)

                    optionButton.connect("item_selected", self, "on_input_device_options_item_selected", [optionButton, commandBindingLayout, gridIndex, sceneName, gameCommandName])
        elif (tabName == kTabInputSubsystem):
            for sceneName in kInputSubsystems:
                add_label_to_layout(layout, sceneName)
                layout.add_child(HSeparator.new())

                var gameScene = kInputSubsystems[sceneName]
                for inputSubsystemName in gameScene:
                    var inputSubsystemResource = gameScene[inputSubsystemName]
                    add_toggle_option_to_layout(layout,
                                                inputSubsystemName,
                                                false,
                                                {
                                                    type = kTabInputSubsystem,
                                                    scene = sceneName,
                                                    resource = inputSubsystemResource
                                                })
        elif (tabName == kTabOutputSubsystem):
            for sceneName in kOutputSubsystems:
                add_label_to_layout(layout, sceneName)
                layout.add_child(HSeparator.new())

                var gameScene = kOutputSubsystems[sceneName]
                for outputSubsystemName in gameScene:
                    var outputSubsystemResource = gameScene[outputSubsystemName]
                    add_toggle_option_to_layout(layout,
                                                outputSubsystemName,
                                                false,
                                                {
                                                    type = kTabOutputSubsystem,
                                                    scene = sceneName,
                                                    resource = outputSubsystemResource
                                                })

    # Set the initial selected element.
    # TODO Support changing tabs without mouse.
    # get_node("TabContainer").get_current_tab_control().grab_focus()
    get_node("ButtonReturnMenu").grab_focus()

    # Set the file extension for the file dialogue.
    var fileDialog = get_node("ButtonCreateProfile/FileDialog")
    fileDialog.add_filter("*.json; Interaction Profile Files")


func add_label_to_layout(layout, text):
    var label = Label.new()
    # label.set_h_size_flags(Control.SIZE_EXPAND_FILL)
    label.set_text(text)
    # label.set_align(Label.ALIGN_CENTER)
    layout.add_child(label)

    return label


func add_toggle_option_to_layout(layout, text, bDefaultValue, toggleParameters):
    var checkButton = CheckButton.new()
    # checkButton.set_h_size_flags(Control.SIZE_EXPAND_FILL)
    checkButton.set_text(text)
    checkButton.set_pressed(bDefaultValue)
    checkButton.connect("toggled", self, "on_button_toggled", [toggleParameters, text])
    layout.add_child(checkButton)

    return checkButton


func add_button_to_layout(layout, text, toggleParameters):
    var button = Button.new()
    # button.set_h_size_flags(Control.SIZE_EXPAND_FILL)
    button.set_text(text)
    toggleParameters["button"] = button
    button.connect("pressed", self, "on_button_pressed", [toggleParameters, text])
    layout.add_child(button)

    return button


# NOTE We assume that everything starts disabled.
func on_button_toggled(bEnabled, bindingParameters, text):
    # TODO Same pattern for all cases; we could refactor the blocks into a
    # helper function.
    if (bindingParameters["type"] == kTabEntity):
        var scene = bindingParameters["scene"]
        var abstractEntity = bindingParameters["target"]
        if (bEnabled):
            if (not m_SelectedContreteEntities[scene][abstractEntity].has(text)):
                m_SelectedContreteEntities[scene][abstractEntity].push_back(text)
        else:
            m_SelectedContreteEntities[scene][abstractEntity].erase(text)
    elif (bindingParameters["type"] == kTabEvent):
        var scene = bindingParameters["scene"]
        var eventName = bindingParameters["target"]
        if (bEnabled):
            if (not m_SelectedConcreteEventHandlers[scene][eventName].has(text)):
                m_SelectedConcreteEventHandlers[scene][eventName].push_back(text)
        else:
            m_SelectedConcreteEventHandlers[scene][eventName].erase(text)
    elif (bindingParameters["type"] == kTabInputSubsystem):
        var scene = bindingParameters["scene"]
        var subsystemName = text
        var subsystemResource = bindingParameters["resource"]
        if (bEnabled):
            if (not m_SelectedInputSubsystems[scene].has(subsystemName)):
                m_SelectedInputSubsystems[scene][subsystemName] = subsystemResource
        else:
            m_SelectedInputSubsystems[scene].erase(subsystemName)
    elif (bindingParameters["type"] == kTabOutputSubsystem):
        var scene = bindingParameters["scene"]
        var subsystemName = text
        var resource = bindingParameters["resource"]
        var subsystemResource = bindingParameters["resource"]
        if (bEnabled):
            if (not m_SelectedOutputSubsystems[scene].has(subsystemName)):
                m_SelectedOutputSubsystems[scene][subsystemName] = subsystemResource
        else:
            m_SelectedOutputSubsystems[scene].erase(subsystemName)
    else:
        assert(!"Invalid type!")


func on_button_pressed(bindingParameters, text):
    if (bindingParameters["type"] == kTabGameCommand):
        m_SelectedInputMappingKeyboardKey = bindingParameters
        set_process_input(true)
    else:
        assert(!"Invalid type!")


func on_input_device_options_item_selected(itemIndex, optionButton, commandBindingLayout, layoutIndex, sceneName, gameCommandName):
    var newItem = null
    var currentDevice = optionButton.get_item_text(itemIndex)
    if (currentDevice == "Keyboard"):
        # Set a default value for the mapping using the default definitions.
        m_SelectedInputMapping[sceneName][gameCommandName] = kDefaultInputMapping[sceneName][gameCommandName]
        # Add a button for displaying current binding and
        # allowing to remap it to another key.
        var currentBinding = m_SelectedInputMapping[sceneName][gameCommandName][1]
        newItem = add_button_to_layout(commandBindingLayout,
                                       currentBinding,
                                       {
                                           type = kTabGameCommand,
                                           scene = sceneName,
                                           target = gameCommandName,
                                           button = null # Added in the function.
                                       })
    elif (currentDevice == "Mouse"):
        newItem = OptionButton.new()
        for buttonName in kInputDeviceMouseOptions:
            newItem.add_item(buttonName[0])

        if (newItem.get_item_count() > 0):
            newItem.connect("item_selected", self, "on_input_device_mouse_item_selected", [sceneName, gameCommandName])
            # Set the first item as the default handler.
            on_input_device_mouse_item_selected(0, sceneName, gameCommandName)

        commandBindingLayout.add_child(newItem)
    elif (currentDevice == "Controller"):
        newItem = OptionButton.new()
        for buttonName in kInputDeviceControllerOptions:
            newItem.add_item(buttonName[0])

        if (newItem.get_item_count() > 0):
            newItem.connect("item_selected", self, "on_input_device_controller_item_selected", [sceneName, gameCommandName])
            # Set the first item as the default handler.
            on_input_device_controller_item_selected(0, sceneName, gameCommandName)

        commandBindingLayout.add_child(newItem)
    elif (currentDevice == "Automation"):
        # Add a combo box with available options.
        var sceneAutomation = kInputDevices[sceneName]["Automation"]
        var commandAutomation = sceneAutomation[gameCommandName]
        newItem = OptionButton.new()
        var eventHandler = m_SelectedInputMapping[sceneName][gameCommandName][1]
        var eventName = commandAutomation["event"]
        for eventHandlerName in commandAutomation["handlers"]:
            newItem.add_item(eventHandlerName)

        if (newItem.get_item_count() > 0):
            newItem.connect("item_selected", self, "on_input_device_automation_item_selected", [newItem, sceneName, gameCommandName, eventName])
            # Set the first item as the default handler.
            on_input_device_automation_item_selected(0, newItem, sceneName, gameCommandName, eventName)

        commandBindingLayout.add_child(newItem)

    commandBindingLayout.move_child(newItem, layoutIndex)

    # Set the correct position (ignore the initial creation).
    # As there is not built-in method to replace the node in a GridContainer, we
    # move the new one to original's position, and remove the latter.
    if (commandBindingLayout.get_child_count() != (layoutIndex + 1)):
        var oldOption = commandBindingLayout.get_child(layoutIndex + 1)
        commandBindingLayout.remove_child(oldOption)
        # oldOption.queue_free()


func on_input_device_automation_item_selected(itemIndex, optionButton, gameScene, gameCommand, automationEvent):
    var eventHandlerName = optionButton.get_item_text(itemIndex)
    m_SelectedInputMapping[gameScene][gameCommand] = ["Automation", automationEvent, eventHandlerName]


func on_input_device_mouse_item_selected(itemIndex, gameScene, gameCommand):
    # var mouseButtonName = optionButton.get_item_text(itemIndex)[0]
    m_SelectedInputMapping[gameScene][gameCommand] = ["Mouse Button", kInputDeviceMouseOptions[itemIndex][1]]


func on_input_device_controller_item_selected(itemIndex, gameScene, gameCommand):
    # var mouseButtonName = optionButton.get_item_text(itemIndex)[0]
    m_SelectedInputMapping[gameScene][gameCommand] = ["Joystick Button", kInputDeviceControllerOptions[itemIndex][1]]
    print(m_SelectedInputMapping[gameScene][gameCommand])


func _input(event):
    if (event.type == InputEvent.KEY):
        get_tree().set_input_as_handled()
        set_process_input(false)

        var gameCommand = m_SelectedInputMappingKeyboardKey["target"]
        var gameCommandButton = m_SelectedInputMappingKeyboardKey["button"]
        var scene = m_SelectedInputMappingKeyboardKey["scene"]

        var bindingString = OS.get_scancode_string(event.scancode)
        gameCommandButton.set_text(bindingString)
        m_SelectedInputMapping[scene][gameCommand] = ["Keyboard", bindingString.to_upper()]

        m_SelectedInputMappingKeyboardKey = null


func create_interaction_profile(interactionProfileFilename):
    # TODO Ensure that handlers can be enabled/disabled; handle dependencies.
    var interactionProfile = {
        InteractionProfile = {
            Scenes = {
                Game = {
                    # Format: key: event name, values: list of event handlers functions.
                    EventSpecialization = m_SelectedConcreteEventHandlers["Game"],
                    ComponentSpecialization = m_SelectedContreteEntities["Game"],
                    InputMapping = m_SelectedInputMapping["Game"],
                    InputSubsystemSpecialization = m_SelectedInputSubsystems["Game"],
                    OutputSubsystemSpecialization = m_SelectedOutputSubsystems["Game"]
                },
                GameEnd = {
                    EventSpecialization = m_SelectedConcreteEventHandlers["GameEnd"],
                    ComponentSpecialization = m_SelectedContreteEntities["GameEnd"],
                    InputMapping = m_SelectedInputMapping["GameEnd"],
                    InputSubsystemSpecialization = m_SelectedInputSubsystems["GameEnd"],
                    OutputSubsystemSpecialization = m_SelectedOutputSubsystems["GameEnd"]
                },
                Menu = {
                    EventSpecialization = m_SelectedConcreteEventHandlers["Menu"],
                    ComponentSpecialization = m_SelectedContreteEntities["Menu"],
                    InputMapping = m_SelectedInputMapping["Menu"],
                    InputSubsystemSpecialization = m_SelectedInputSubsystems["Menu"],
                    OutputSubsystemSpecialization = m_SelectedOutputSubsystems["Menu"]
                }
            },
        }
    }

    var file = File.new()
    # if (not file.file_exists(interactionProfileFilename)):
    var error = file.open(interactionProfileFilename, File.WRITE)
    assert(error == OK)
    file.store_line(interactionProfile.to_json())
    file.close()


func _on_ButtonCreateProfile_pressed():
    var fileDialog = get_node("ButtonCreateProfile/FileDialog")
    fileDialog.invalidate()
    # var defaultDirectory = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)
    # if (defaultDirectory.empty()):
    #     defaultDirectory = OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP)
    #     fileDialog.set_current_dir(defaultDirectory)
    #     fileDialog.invalidate()
    #     # fileDialog.show_modal(true)
    fileDialog.popup_centered_ratio(0.75)


func _on_FileDialog_file_selected(path):
    create_interaction_profile(path)


func _on_ButtonReturnMenu_pressed():
    g_SceneManager.change_scene_to(g_SceneManager.kMenu)
