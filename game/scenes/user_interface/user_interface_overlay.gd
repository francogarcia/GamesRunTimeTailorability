extends CanvasLayer

# NOTE To change the quantity of simultaneous sounds, change "Polyphony"
# property.

# NOTE The original sounds were converted to WAV using the following script:
# #!/bin/bash
# INPUT_FORMAT=ogg
# OUTPUT_FORMAT=wav
# PATH=game/scenes/usual_game/assets/sounds/messersm/
# pushd ${PATH}
# for file in *.${INPUT_FORMAT}; do
#     extension="${file##*.}"
#     fileName="${file%.*}"
#     ffmpeg -i ${file} ${fileName}.${OUTPUT_FORMAT}
# done
# popd

# NOTE The laser sounds in the sample library use Kenney's versions of the
# sounds.

# const kSoundExplosion1 = "explosion1"
# const kSoundExplosion2 = "explosion2"
# const kSoundExplosion3 = "explosion3"
# const kSoundExplosion4 = "explosion4"
# const kSoundExplosion5 = "explosion5"
# const kSoundExplosion6 = "explosion6"

# const kSoundLaser1 = "laser1"
# const kSoundLaser2 = "laser2"
# const kSoundLaser3 = "laser3"
# const kSoundLaser4 = "laser4"
# const kSoundLaser5 = "laser5"
# const kSoundLaser6 = "laser6"
# const kSoundLaser7 = "laser7"
# const kSoundLaser8 = "laser8"
# const kSoundLaser9 = "laser9"

# const kSoundZap1 = "zap1"
# const kSoundZap2 = "zap2"
# const kSoundZap3 = "zapThreeToneDown"
# const kSoundZap4 = "zapThreeToneUp"
# const kSoundZap5 = "zapTwoTone"
# const kSoundZap6 = "zapTwoTone2"


var m_SamplePlayer = null
var m_StreamPlayer = null

var m_CurrentStream = null

# Music resources.
# var m_MainMusic = null

# Sound effects
var m_SoundEffects = null
var m_Instructions = null

# Interaction Profile
var m_CustomInteractionProfilePath = null


func _ready():
    m_SamplePlayer = get_node("SamplePlayer2D")
    m_StreamPlayer = get_node("StreamPlayer")

    # TODO Set default values for volume. Also set the value in play_sound_effect().
    # m_SamplePlayer.set_default_volume(1.0)
    # m_StreamPlayer.set_volume(1.0)

    # Load music resources.
    # m_MainMusic = load("res://file.ogg")

    # Currently playing sounds
    m_SoundEffects = []
    m_Instructions = []

    # Pause menu.
    InputMap.add_action("toggle_pause")
    g_Tailoring.register_keyboard_key("toggle_pause", "escape")

    # Interaction profile settings.
    register_interaction_profiles()
    m_CustomInteractionProfilePath = null
    set_fixed_process(true)


# Destructor.
func _notification(notification):
   if notification == NOTIFICATION_PREDELETE:
       unregister_interaction_profiles()
       InputMap.erase_action("toggle_pause")


func _fixed_process(delta):
    if (Input.is_action_just_pressed("toggle_pause")):
        toggle_pause()

    if (Input.is_action_just_pressed("tailor")):
        g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring...")
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    if (Input.is_action_just_pressed("untailor")):
        g_SceneManager.get_user_interface_overlay().set_text_description("Untailoring...")
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    if ((Input.is_action_just_pressed("load_profile_usual")) or
        (Input.is_action_just_pressed("load_profile_usual_variation")) or
        (Input.is_action_just_pressed("load_profile_usual_text_description")) or
        (Input.is_action_just_pressed("load_profile_usual_sounds")) or
        (Input.is_action_just_pressed("load_profile_usual_automated_input")) or
        (Input.is_action_just_pressed("load_profile_usual_automated_input_partial")) or
        (Input.is_action_just_pressed("load_profile_custom"))):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())

        if (Input.is_action_just_pressed("load_profile_usual")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game.json")
        elif (Input.is_action_just_pressed("load_profile_usual_variation")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with alternate assets...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_variation.json")
        elif (Input.is_action_just_pressed("load_profile_usual_text_description")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with text descriptions...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_text_description.json")
        elif (Input.is_action_just_pressed("load_profile_usual_sounds")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with sounds...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_sound.json")
        elif (Input.is_action_just_pressed("load_profile_usual_automated_input")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with input automation...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_automated_input.json")
        elif (Input.is_action_just_pressed("load_profile_usual_automated_input_partial")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with partial input automation...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_hybrid_input.json")
        elif (Input.is_action_just_pressed("load_profile_custom")):
            var customInteractionProfile = get_custom_interaction_profile_path()
            if (customInteractionProfile != null):
                g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to profile defined in the pause menu...")
                g_Tailoring.load_and_set_interaction_profile(customInteractionProfile)
            else:
                g_SceneManager.get_user_interface_overlay().set_text_description("Before using this, pause the game and load a custom profile.")
        else:
            asssert(!"Invalid profile!")

        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())


# Play a sound effect, and return its ID (voiceID).
func play_sound_effect(soundKey):
    var voiceID = m_SamplePlayer.play(soundKey)
    # m_SamplePlayer.set_volume(voiceID, 1.0)
    m_SoundEffects.push_back(voiceID)
    return voiceID


func stop_sound_effect(voiceID):
    m_SamplePlayer.stop(voiceID)


func stop_music():
    if (m_StreamPlayer.is_playing()):
        m_StreamPlayer.stop()


func _play_music():
    stop_music()

    m_StreamPlayer.set_stream(m_CurrentStream)
    m_StreamPlayer.set_loop(true)

    m_StreamPlayer.play()


# func play_main_music():
#     m_CurrentStream = m_MainMusic
#     _play_music()


func set_text_description(description):
    get_node("Label").set_text(description)


func get_custom_interaction_profile_path():
    return m_CustomInteractionProfilePath


func set_custom_interaction_profile_path(interactionProfileFilename):
    assert(interactionProfileFilename != null)

    m_CustomInteractionProfilePath = interactionProfileFilename


# NOTE This is for testing tailoring/untailoring during the game.
func register_interaction_profiles():
    InputMap.add_action("tailor")
    InputMap.add_action("untailor")
    InputMap.add_action("load_profile_usual")
    InputMap.add_action("load_profile_usual_variation")
    InputMap.add_action("load_profile_usual_text_description")
    InputMap.add_action("load_profile_usual_sounds")
    InputMap.add_action("load_profile_usual_automated_input")
    InputMap.add_action("load_profile_usual_automated_input_partial")
    InputMap.add_action("load_profile_custom")

    g_Tailoring.register_keyboard_key("tailor", "f1")
    g_Tailoring.register_keyboard_key("untailor", "f2")
    g_Tailoring.register_keyboard_key("load_profile_usual", "f3")
    g_Tailoring.register_keyboard_key("load_profile_usual_variation", "f4")
    g_Tailoring.register_keyboard_key("load_profile_usual_text_description", "f5")
    g_Tailoring.register_keyboard_key("load_profile_usual_sounds", "f6")
    g_Tailoring.register_keyboard_key("load_profile_usual_automated_input", "f9")
    g_Tailoring.register_keyboard_key("load_profile_usual_automated_input_partial", "f10")

    g_Tailoring.register_keyboard_key("load_profile_custom", "f12")


func unregister_interaction_profiles():
    InputMap.erase_action("tailor")
    InputMap.erase_action("untailor")
    InputMap.erase_action("load_profile_usual")
    InputMap.erase_action("load_profile_usual_variation")
    InputMap.erase_action("load_profile_usual_text_description")
    InputMap.erase_action("load_profile_usual_sounds")
    InputMap.erase_action("load_profile_usual_automated_input")
    InputMap.erase_action("load_profile_usual_automated_input_partial")
    InputMap.erase_action("load_profile_custom")


func toggle_pause():
    if (g_SceneManager.get_current_scene_key() == g_SceneManager.kGame):
        if (get_node("PauseMenu").is_hidden()):
            pause_game()
        else:
            resume_game()


func pause_game():
    get_tree().set_pause(true)

    # get_node("PauseMenu").set_size(OS.get_window_size())
    get_node("PauseMenu").popup_centered_ratio(0.75)
    # get_node("PauseMenu").show()


func resume_game():
    get_node("PauseMenu").hide()

    get_tree().set_pause(false)


func _on_ButtonResume_pressed():
    resume_game()
